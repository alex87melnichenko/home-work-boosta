<?php

use yii\db\Migration;

/**
 * Class m211125_162738_books
 */
class m211125_162738_books extends Migration
{
    /**
     * {@inheritdoc}
     */
    public function safeUp()
    {
        $this->execute("
          CREATE TABLE books (
                id INTEGER NOT NULL PRIMARY KEY AUTOINCREMENT,
                title VARCHAR(255) NOT NULL,
                description VARCHAR(500) NOT NULL,
                author varchar(255) NOT NULL
          );
        ");
    }

    /**
     * {@inheritdoc}
     */
    public function safeDown()
    {
        $this->execute("
          DROP TABLE books);
        ");
    }

}
