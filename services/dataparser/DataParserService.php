<?php
namespace app\services\dataparser;

use app\services\dataparser\parsers\ArrayDataParser;
use app\services\dataparser\parsers\CsvDataParser;
use app\services\dataparser\parsers\DataParserInterface;
use app\services\dataparser\parsers\JsonDataParser;
use app\services\dataparser\parsers\YamlDataParser;

use yii\web\UploadedFile;

class DataParserService {

    /**
     * @var DataParserInterface
     * */
    protected $parser;

    /**
     * @var mixed
     * */
    protected $data;


    /**
     * Закрываем конструктор
     * */
    private function __construct()
    {
    }


    /**
     * @param $data
     * @return static
     * */
    public static function factory($data)
    {
        if ($data instanceof UploadedFile) {

            /*
             * Здесь по метаданным файла определяем тип контента файла и устанавливаем парсер
             * Можно по расширению файла было смотреть
             * */
            switch ($data->type) {

                case 'application/json':
                    $parserService = new JsonDataParser;
                    break;
                case 'application/x-yaml':
                    $parserService = new YamlDataParser;
                    break;
                case 'text/csv':
                    $parserService = new CsvDataParser;
                    break;
                default:
                    throw new InvalidDataException("Unsupported file format. Format: " . $data->type);
            }
        } elseif (is_array($data)) {

            /*
             * Вилка для кейса когда данные пришли в виде массива с html формы
             * */
            $parserService = new ArrayDataParser;
        } else {

            throw new \InvalidArgumentException("Unsupported data format. Allowed array|UploadedFile");
        }

        return (new static)
            ->setParser($parserService)
            ->setData($data);
    }


    private function setParser(DataParserInterface $parser)
    {
        $this->parser = $parser;

        return $this;
    }


    private function setData($inputData)
    {
        $this->data = $inputData;

        return $this;
    }


    public function toArray()
    {
        return $this->parser->parse($this->data);
    }
}