<?php
namespace app\services\dataparser\parsers;

use app\services\dataparser\InvalidDataFormatException;

class YamlDataParser extends DataParserAbstract implements DataParserInterface {

    public function parse($data)
    {
        $rawData = $this->getRawContent($data);

        try {
            //Странно, но парсер кидает исключение, хотя в документации об этом не сказано
            $yamlDecoded = yaml_parse($rawData);

        } catch (\Exception $e) {

            throw new InvalidDataFormatException("Invalid YAML format file content");
        }

        return $yamlDecoded;
    }

}