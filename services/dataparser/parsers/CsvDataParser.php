<?php
namespace app\services\dataparser\parsers;

class CsvDataParser extends DataParserAbstract implements DataParserInterface {

    public function parse($data)
    {
        $rawData = $this->getRawContent($data);

        $lines = explode(PHP_EOL, $rawData);
        $parsedData = [];

        foreach ($lines as $line) {

            $parsedLine = str_getcsv($line);
            $parsedData[] = [
                'title'         => $parsedLine[0] ?? '',
                'description'   => $parsedLine[1] ?? '',
                'author'        => $parsedLine[2] ?? '',
            ];
        }

        return $parsedData;
    }
}