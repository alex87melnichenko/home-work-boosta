<?php
namespace app\services\dataparser\parsers;

use yii\base\InvalidArgumentException;

interface DataParserInterface {

    /**
     * @param mixed
     * @throws InvalidArgumentException|
     * */
    public function parse($data);
}