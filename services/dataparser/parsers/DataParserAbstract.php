<?php
namespace app\services\dataparser\parsers;

abstract class DataParserAbstract {

    protected function getRawContent($data)
    {
        return file_get_contents($data->tempName);
    }
}