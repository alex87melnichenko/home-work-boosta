<?php
namespace app\services\dataparser\parsers;

use app\services\dataparser\InvalidDataFormatException;

class JsonDataParser extends DataParserAbstract implements DataParserInterface {

    public function parse($data)
    {
        $rawData = $this->getRawContent($data);

        $jsonDecoded =  json_decode($rawData, true);

        if (is_null($jsonDecoded)) {

            throw new InvalidDataFormatException("Invalid JSON format file content");
        }

        return $jsonDecoded;
    }

}