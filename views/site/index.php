<?php
use yii\bootstrap4\Nav;
use yii\helpers\Html;
/**
 * @var $this yii\web\View
 * @var $form app\models\ImportBookForm
 */


$this->title = 'Import Book Data';

?>
<div class="site-index">

    <div class="body-content">
        <nav class="navbar navbar-expand-md navbar-dark bg-dark fixed-top">
            <div class="container">
                <?php
                echo Nav::widget([
                    'options' => ['class' => 'navbar-nav'],
                    'items' => [
                        ['label' => 'Import', 'url' => ['/site/index']],
                        ['label' => 'List', 'url' => ['/site/list']],

                    ],
                ]);
                ?>
            </div>
        </nav>

        <h1><?= $this->title?></h1>
        <div class="row">
            <div class="col-lg-12">
                <form id="input-form" action="" method="post" enctype="multipart/form-data">
                    <div class="col-sm-10">
                        <div class="form-check">
                            <?= Html::activeRadio(
                                $form,
                                'sourceType',
                                [
                                    'class' => 'form-check-input',
                                    'value' => $form::SOURCE_TYPE_FORM,
                                    'label' => 'Form Data',
                                    'uncheck'=> false
                                ]); ?>
                        </div>
                        <div class="form-check">
                            <?= Html::activeRadio(
                                $form,
                                'sourceType',
                                [
                                    'class' => 'form-check-input',
                                    'value' => $form::SOURCE_TYPE_FILE,
                                    'label' => 'File',
                                    'uncheck'=> false
                                ]); ?>
                        </div>

                    </div>

                    <div class="form-row" id="row-file" style="<?= ($form->sourceType != $form::SOURCE_TYPE_FILE) ? 'display:none' : ''?>">
                        <div class="form-group">
                            <label>
                                Chose file to import
                                <small>(*.csv, *.json, *.yaml)</small>
                            </label>
                            <input type="file" name="importFile" class="form-control-file" accept=".csv,.json,.yaml">
                        </div>
                    </div>

                    <div class="form-row line" id="row-form" style="<?= ($form->sourceType != $form::SOURCE_TYPE_FORM) ? 'display:none' : ''?>">
                        <div class="form-group col-md-1 text-muted row-number">
                            #1
                        </div>
                        <div class="form-group col-md-3">
                            <input type="text" name="books[1][title]" class="form-control" placeholder="Title">
                        </div>
                        <div class="form-group col-md-3">
                            <input type="text" name="books[1][description]" class="form-control" placeholder="Description">
                        </div>
                        <div class="form-group col-md-3">
                            <input type="text" name="books[1][author]" class="form-control" placeholder="Author">
                        </div>
                    </div>

                    <div class="row">
                        <div class="col-lg-12">
                            <button type="button" class="btn btn-success btn-sm btn-flat" id="add">Add row</button>
                            <button type="submit" class="btn btn-primary btn-sm btn-flat" id="add">Submit</button>
                        </div>
                    </div>
                </form>
            </div>
        </div>
    </div>
</div>

<?php

$sourceFile =$form::SOURCE_TYPE_FILE;

$JS = <<<JS
    $('#add').click(function(){
            let \$rowFirst   = \$('.line').first();
            let \$rowNew     = \$rowFirst.clone();
            let rowNewNumber = \$.find('.row-number').length + 1;

            \$rowNew.find('.row-number').text('#' + rowNewNumber);
            \$rowNew.find('[name*=title]').attr('name', 'books['+ rowNewNumber +'][title]')
            \$rowNew.find('[name*=description]').attr('name', 'books['+ rowNewNumber +'][description]')
            \$rowNew.find('[name*=author]').attr('name', 'books['+ rowNewNumber +'][author]')

            \$('#input-form .line').last().after(\$rowNew);
        });

        $('[name=sourceType]').click(function(){
            if ($(this).val() == {$sourceFile}) {
                $('#row-file').show();
                $('#row-form').hide();
            } else {
                $('#row-file').hide();
                $('#row-form').show();
            }
        })
JS;


$this->registerJs($JS);
