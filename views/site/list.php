<?php
use yii\bootstrap4\LinkPager;
use yii\bootstrap4\Nav;
/**
 * @var $this yii\web\View
 * @var $dataProvider \yii\data\ActiveDataProvider
 *
 */


$this->title = 'Books';

?>
<div class="site-index">

    <div class="body-content">
        <nav class="navbar navbar-expand-md navbar-dark bg-dark fixed-top">
            <div class="container">
            <?php
            echo Nav::widget([
                'options' => ['class' => 'navbar-nav'],
                'items' => [
                    ['label' => 'Import', 'url' => ['/site/index']],
                    ['label' => 'List', 'url' => ['/site/list']],

                ],
            ]);
            ?>
            </div>
        </nav>

        <h1><?= $this->title?></h1>
        <div class="row">
            <div class="col-lg-12">
                <table class="table table-condensed">
                    <thead>
                        <tr>
                            <th scope="col">#</th>
                            <th scope="col">Title</th>
                            <th scope="col">Description</th>
                            <th scope="col">Author</th>
                        </tr>
                    </thead>
                    <tbody>
                        <?php foreach ($dataProvider->getModels() as $model): ?>
                            <tr>
                                <td><?=$model['id']?></td>
                                <td><?=$model['title']?></td>
                                <td><?=$model['description']?></td>
                                <td><?=$model['author']?></td>
                            </tr>
                        <?php endforeach; ?>
                    </tbody>
                </table>
                <?php
                if ( ! $dataProvider->getModels()) {

                    $widget = '';
                } else {

                    $pager['pagination'] = $dataProvider->getPagination();
                    $pager['view'] = $this;

                    $widget = LinkPager::widget($pager);
                }
                echo '<nav aria-label="Page navigation example">'.$widget.'</nav>'?>
            </div>
        </div>
    </div>
</div>
