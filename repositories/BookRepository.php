<?php
namespace app\repositories;

use app\models\Book;

class BookRepository extends BaseRepositoryAbstract
{

    /**
     * @return string|\yii\db\ActiveRecord
     * */
    public function getModelClass()
    {
        return Book::class;
    }
}
