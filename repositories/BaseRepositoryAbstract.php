<?php
namespace app\repositories;

use yii\db\ActiveRecord;

abstract class BaseRepositoryAbstract
{

    /**
     * @return string|\yii\db\ActiveRecord
     * */
    abstract function getModelClass();

    public function findByParams($params, $options)
    {
        $modelClass = $this->getModelClass();

        $activeQuery = $modelClass::find();

        foreach ($params as $condition) {

            $activeQuery->andWhere($condition);
        }

        foreach (['limit', 'offset', 'indexBy', 'asArray'] as $option) {

            if (array_key_exists($option, $options)) {

                $activeQuery->$option($options[$option]);
            }
        }

        return $activeQuery->all();
    }


    public function save($row)
    {
        return $this->saveBatch([$row]);
    }

    public function saveBatch($rows)
    {
        if (empty($rows)) {

            return 0;
        }

        $columns = array_keys(reset($rows));
        /**
         * @var ActiveRecord|string $class
         */
        $class = $this->getModelClass();

        $table = $class::tableName();

        return $class::getDb()
            ->createCommand()
            ->batchInsert($table, $columns, $rows)
            ->execute();
    }
}