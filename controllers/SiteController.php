<?php

namespace app\controllers;

use Yii;
use yii\data\ActiveDataProvider;
use yii\web\Controller;

use app\models\forms\ImportBookForm;
use app\repositories\BookRepository;

class SiteController extends Controller
{
    public $enableCsrfValidation = false;


    public function actionIndex()
    {
        $form = new ImportBookForm;

        if (Yii::$app->request->isPost) {

            if ($form->load(Yii::$app->request->post()) && $form->validate()) {

                $count = $form->save();
                Yii::$app->session->addFlash('success', "Inserted {$count} items");

                return  $this->redirect('/site/list');
            } else {

                Yii::$app->session->addFlash('error', implode('<br>', $form->getFirstErrors()));
            }
        }

        return $this->render('index', [
            'form' => $form
        ]);
    }

    public function actionList()
    {
        $modelClass = (new BookRepository)->getModelClass();

        return $this->render('list', [
            'dataProvider' => new ActiveDataProvider([
                'query' => $modelClass::find(),
                'sort' => [
                    'defaultOrder' => ['id' => SORT_DESC],
                ],
                'pagination' => [
                    'defaultPageSize' => 10
                ]
            ])
        ]);
    }
}
