<?php

namespace app\models\forms;

use app\repositories\BookRepository;
use app\services\dataparser\DataParserService;
use app\services\dataparser\InvalidDataException;
use app\services\dataparser\InvalidDataFormatException;

use yii\base\Model;
use yii\web\UploadedFile;

class ImportBookForm extends Model
{
    /**
     * @var UploadedFile
    */
    public $importFile;

    /**
     * @var array
     * */
    public $books;

    /**
     * @var int
     * */
    public $sourceType = self::SOURCE_TYPE_FORM;

    const SOURCE_TYPE_FORM = 1;
    const SOURCE_TYPE_FILE = 2;


    /**
     * @return array the validation rules.
     */
    public function rules()
    {
        return [
            // name, email, subject and body are required
            ['sourceType', 'integer'],
            ['books', 'each', 'rule' => [function() {

                if (empty($this->books['title'])
                    || empty($this->books['description'])
                    || empty($this->books['author'])) {

                    $this->addError('books', 'Please fill all fields. Fields: title, description, author');
                }

                if (!is_string($this->books['title'])
                    || !is_string($this->books['description'])
                    || !is_string($this->books['author'])) {

                    $this->addError('books', 'All fields must be a string. Fields: title, description, author');
                }
            }]]
        ];
    }

    public function formName()
    {
        return '';
    }


    public function beforeValidate()
    {
        /*
         * Прогоняни данные через парсер,
         * что бы модель формы работа с ними к сданными единого формата
         * */
        $this->importFile = UploadedFile::getInstance($this, 'importFile');

        try {

            $this->parseData();
        } catch (InvalidDataException $e) {

            $this->addError('importFile', $e->getMessage());
        } catch (InvalidDataFormatException $e) {

            $this->addError('importFile', 'Failed to parse imported data');
        }

        //Если все прошло гладко в парсинге то продолжаем работу(валидируем данные)
        return !$this->hasErrors() && parent::beforeValidate();
    }

    /**
     * @return void
     */
    protected function parseData()
    {
        switch ($this->sourceType) {

            case self::SOURCE_TYPE_FILE:
                $parseData = $this->importFile;
                break;

            default:
                $parseData = $this->books;
        }
        $dataParser = DataParserService::factory($parseData);

        $this->books = $dataParser->toArray();
    }



    public function save()
    {
        $saveData = array_map(function ($element) {

            return [
                'title'         => $element['title'],
                'description'   => $element['author'],
                'author'        => $element['author'],
            ];
        }, $this->books);

        return (new BookRepository)->saveBatch($saveData);
    }
}
